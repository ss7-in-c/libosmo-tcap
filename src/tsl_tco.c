/* ITU-T Q.77x TCAP / TCO - Transoaction CoOrdinatior */

/* (C) 2010 by Harald Welte <laforge@gnumonks.org>
 * (C) 2010 by On-Waves
 *
 * All Rights Reserved
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <unistd.h>
#include <errno.h>
#include <netinet/in.h>

#include <osmocom/core/msgb.h>
#include <osmocom/core/logging.h>
#include <osmocom/tcap/TCMessage.h>

#include "tcap.h"


uint32_t tid_from_octetstr(struct OCTET_STRING *octstr)
{
	if (octstr->size != sizeof(uint32_t))
		fprintf(stderr, "Transaction ID length %u != violates spec\n", octstr->size);

	return ntohl(*(uint32_t *)octstr->buf);
}


/* N-UNITDATA.ind from SCCP / SUA */
int tcap_tco_n_unitdata_ind(struct tcap_transport_entity *se, struct msgb *msg)
{
	int rc = 0;
	asn_dec_rval_t rv;
	struct TCMessage *tcmsg = NULL;
	struct tcap_transaction *tt;
	uint32_t dest_tid;

	fprintf(stdout, "N-UNITDATA.ind(%s):\n", osmo_hexdump(msg->data, msg->len));

	rv = ber_decode(NULL, &asn_DEF_TCMessage, (void **) &tcmsg, msg->data, msg->len);
	if (rv.code != RC_OK) {
		fprintf(stderr, "error during BER_decode() RC=%d consumed=%u\n", rv.code, rv.consumed);
		return rc;
	}
	xer_fprint(stdout, &asn_DEF_TCMessage, tcmsg);

	switch (tcmsg->present) {
	case TCMessage_PR_unidirectional:
		tt = tcap_transaction_alloc();
		/* Send TR-UNI.ind to CSL */
		rc = tcap_csl_tr_uni_ind(tt, tcmsg, msg);
		break;
	case TCMessage_PR_begin:
		/* Assign local transaction ID */
		tt = tcap_transaction_alloc();
		/* FIXME: Is TID == no TID ? */
		/* Send BEGIN.recv to TSM */
		dialg_by_trans(tt)->transp_ent = se;
		rc = tcap_tsm_begin_rcvd(tt, tcmsg, msg);
		break;
	case TCMessage_PR_continue:
		dest_tid = tid_from_octetstr(&tcmsg->choice.Continue.dtid);
		tt = tcap_transaction_by_local_tid(dest_tid);
		/* FIXME: Check if DTID is assigned */
		if (!tt)
			goto out_err_no_tid;
		/* Send CONTINUE.recv to TSM */
		rc = tcap_tsm_continue_rcvd(tt, tcmsg, msg);
		break;
	case TCMessage_PR_end:
		dest_tid = tid_from_octetstr(&tcmsg->choice.end.dtid);
		tt = tcap_transaction_by_local_tid(dest_tid);
		/* FIXME: Check if DTID is assigned */
		if (!tt)
			goto out_err_no_tid;
		/* Send END.recv to TSM */
		rc = tcap_tsm_end_rcvd(tt, tcmsg, msg);
		break;
	case TCMessage_PR_abort:
		dest_tid = tid_from_octetstr(&tcmsg->choice.abort.dtid);
		tt = tcap_transaction_by_local_tid(dest_tid);
		/* FIXME: Check if DTID is assigned */
		if (!tt)
			goto out_err_no_tid;
		/* Send ABORT.recv to TSM */
		rc = tcap_tsm_abort_rcvd(tt, tcmsg, msg);
		break;
	default:
		/* FIXME: Case "2" */
		break;
	}

	/* Release the parsed structure */
	asn_DEF_TCMessage.free_struct(&asn_DEF_TCMessage, tcmsg, 0);

	return rc;

out_err_no_tid:
	fprintf(stderr, "TCMessage for unknown TID 0x%08x\n", dest_tid);
	return -EIO;
}

/* N-NOTICE.ind from SCCP / SUA */
int tcap_tco_n_notice_ind(struct tcap_transport_entity *se)
{
	/* Generate TR-NOTICE.ind to CSL */
	//return tcap_csl_tr_notice_ind();
}

/* TSM stopped (TSM -> TCO) */
int tcap_tco_tsm_stopped_ind(struct tcap_transaction *tt)
{
	/* FIXME: Free Transaction ID */
}

/* TR-UNI.req from CSL */
int tcap_tco_tr_uni_req(struct tcap_transaction *tt, struct TCMessage *tcmsg)
{
	/* FIXME: Assemble TR-portion of UNI message */
	/* FIXME: N-UNITDATA.req to SCCP/SUA */
}

/* TR-BEGIN.req from CSL */
int tcap_tco_tr_begin_req(struct tcap_transaction *tt, struct TCMessage *tcmsg)
{
	/* hand-off to TSM */
	/* generate BEGIN transaction to TSM */
	return tcap_tsm_begin_trans(tt, tcmsg);
}

/* TR-CONTINUE.req from CSL */
int tcap_tco_tr_continue_req(struct tcap_transaction *tt, struct TCMessage *tcmsg)
{
	/* generate CONTINUE transaction to TSM */
	return tcap_tsm_continue_trans(tt, tcmsg);
}

/* TR-END.req from CSL */
int tcap_tco_tr_end_req(struct tcap_transaction *tt, struct TCMessage *tcmsg)
{
	/* generate END transaction to TSM */
	return tcap_tsm_end_trans(tt, tcmsg);
}

/* TR-U-ABORT.req from CSL */
int tcap_tco_tr_u_abort_req(struct tcap_transaction *tt, struct TCMessage *tcmsg)
{
	/* generate ABORT transaction to TSM */
	return tcap_tsm_abort_trans(tt, tcmsg);
}
