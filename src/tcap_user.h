#ifndef _TCAP_USER_H
#define _TCAP_USER_H

/* (C) 2010 by Harald Welte <laforge@gnumonks.org>
 * (C) 2010 by On-Waves
 *
 * All Rights Reserved
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <sys/socket.h>

#include <osmocom/core/select.h>
#include <osmocom/core/write_queue.h>

#include <asn1c/OBJECT_IDENTIFIER.h>
#include <osmocom/tcap/Parameter.h>
#include <osmocom/tcap/OPERATION.h>

enum tcap_transport_entity_type {
	SCXP_T_UDP,		/* TCAP over UDP */
	SCXP_T_SUA_SCTP,	/* TCAP over SUA over SCTP */
	SCXP_T_SCCP_IPA,	/* TCAP over SCCP over IPA */
	SCXP_T_SCCP_M3UA_SCTP,	/* TCAP over SCCP over M3UA over SCTP */
};
/* One entity of the underlying network protocol */
struct tcap_transport_entity {
	struct llist_head list;

	/* common parts */
	struct sockaddr_storage local_addr;
	struct sockaddr_storage remote_addr;

	/* type of underlying transport */
	enum tcap_transport_entity_type type;
	union {
		struct {
			struct osmo_wqueue write_queue;
		} udp;
		struct {
		} sccp_ipa;
		struct {
		} sua_sctp;
	};
};

struct tcap_obj_ident {
	uint32_t arc[16];
	unsigned int num_arcs;
};

struct tcap_user_info {
	/* 272 bytes is the MTU of classic SS7 links */
	uint8_t data[272];
	uint32_t data_len;
};

extern struct tcap_transport_entity *tcap_transp_udp_create(struct sockaddr_storage *local_addr);

/* Call-back to user from other points in the stack */

/* metadata associated with a dialogue indication primitive */
struct tcap_dialg_ind {
	/* Dialogue ID to which this primitive relates */
	uint32_t dialg_id;
	/* Reference to the user-provided TCAP transport entity */
	struct tcap_transport_entity *transp_ent;
	uint32_t components_present:1,
		 prearranged_end:1,
		 app_ctx_present:1,
		 user_info_present:1;
	/* Reason/Cause in case of some error */
	uint32_t reason;
	/* Application Context Name as OID */
	struct tcap_obj_ident app_ctx_name;
	/* User information (application dialogue data) */
	struct tcap_user_info user_info;
};

/* metadata associated with a component indication primitive */
struct tcap_component_ind {
	/* public */
	uint32_t dialg_id;		/* Dialogue ID */
	int8_t invoke_id;		/* Invoke ID */
	uint8_t op_class;		/* Operation Class (1-4) */
	int8_t _linked_id;		/* actual storage for linked ID */
	int8_t *linked_id;		/* Linked ID */
	struct {
		int is_global:1;	/* is it global (1) or local (0) */
		union {
			/* Global Operation (OID) */
			struct tcap_obj_ident global;
			/* Local Operation */
			long local;
		};
	} operation;
	struct tcap_user_info parameter;
	int last_component;		/* is this the last component in the msg? */
	uint32_t timeout_secs;	/* Timeout in seconds */
	union {
		struct {
			int is_private:1;	/* Is this a private (1) or national (0) error code? */
			long err;
		} error;
		struct {
			uint8_t problem_type;
			long problem_code;
		} reject;
	};
};

enum tcap_primitive {
	/* dialogue handling primitives */
	_TCAP_PR_DIALG_BASE	= 0,
	TCAP_PR_TC_UNI,
	TCAP_PR_TC_BEGIN,
	TCAP_PR_TC_CONTINUE,
	TCAP_PR_TC_END,
	TCAP_PR_TC_U_ABORT,
	TCAP_PR_TC_NOTICE,
	/* component handling primitives */
	_TCAP_PR_COMP_BASE	= 10,
	TCAP_PR_TC_INVOKE,
	TCAP_PR_TC_RESULT_L,
	TCAP_PR_TC_RESULT_NL,
	TCAP_PR_TC_U_ERROR,
	TCAP_PR_TC_U_REJECT,
	TCAP_PR_TC_CANCEL,
	TCAP_PR_TC_TIMER_RESET,
	TCAP_PR_TC_L_REJECT,
	TCAP_PR_TC_R_REJECT,
	TCAP_PR_TC_P_ABORT,
};

/* primitive structure to pass primitives between TCAP-User and TCAP Layer */
struct tcap_prim_buf {
	/* Dummy list head structure for the user.  libosmo-tcap doesn't use it */
	struct llist_head list;

	/* private reference of the caller */
	unsigned long user_ref;
	unsigned long user_ref2;

	/* The actual primitive number */
	enum tcap_primitive prim;
	union {
		struct tcap_dialg_ind dialg;
		struct tcap_component_ind comp;
	};
};

struct scxp_entity;

const char *tcap_prim_name(enum tcap_primitive prim);


/* callbacks to application code regarding various INDICATIONs */
extern int tcap_user_ind_cb(struct tcap_prim_buf *tcpb);
/* application wants to issue a REQUEST type primitive */
extern int tcap_user_req(struct tcap_prim_buf *tcpb);

#endif /* TCAP_USER_H */
